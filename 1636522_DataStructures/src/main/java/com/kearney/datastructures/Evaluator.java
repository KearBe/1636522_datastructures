package com.kearney.datastructures;

import com.kearney.datastructures.exceptions.*;
import java.util.*;
import java.math.*;

/**
 *
 * @author Benjamin Kearney
 */
public class Evaluator {
    
    private String infix;
    private Deque<Character> stack = new ArrayDeque<Character>();
    private List<String> postfix = new ArrayList<String>();
    private Deque<Double> calcstack = new ArrayDeque<Double>();
    
    /**
     * The constructor takes as input a queue representing the infix expression
     * and converts it to a string to simplify the conversion to postfix
     * 
     * @author Benjamin Kearney
     * @param Queue<String> expression 
     */
    public Evaluator(Queue<String> expression) throws DivideByZeroException, 
            NonMatchingParenthesisException, InvalidExpressionException, 
            NonBinaryExpressionException{
        infix = "";
        for(String s : expression){
            if(s.charAt(0) == '-' && s.length() > 1){
                infix += "(0"+s+")";
            }else{
                infix += s;
            }
        }
        devideByZeroCheck();
        nonMatchingParenthesisCheck();
        invalidExpressionCheck();
        convertToPostfix();
        nonBinaryCheck();
    }
    
    /**
     * calculates the postfix expression
     * 
     * @author Benjamin Kearney
     * @return result
     */
    public String getResult() throws DivideByZeroException{
        for(String s : postfix){
            if(Character.isDigit(s.charAt(0))){
                calcstack.addLast(Double.parseDouble(s));
            }else {
                double tempResult = 0;
                double temp;

                switch(s){
                    case "+": temp = calcstack.removeLast();
                              tempResult = calcstack.removeLast() + temp;
                              break;
                    case "-": temp = calcstack.removeLast();
                              tempResult = calcstack.removeLast() - temp;
                              break;
                    case "*": temp = calcstack.removeLast();
                              tempResult = calcstack.removeLast() * temp;
                              break;
                    case "/": temp = calcstack.removeLast();
                    if(temp == 0){
                        throw new DivideByZeroException();
                    }
                              tempResult = calcstack.removeLast() / temp;
                              break;
                }
                calcstack.addLast(tempResult);
            }
        }
        //.setScale(3, BigDecimal.ROUND_HALF_UP)
        return new BigDecimal(calcstack.removeLast()).toString();
    }
    
    /**
     * converts the infix expression to postfix by evaluating each character.
     * if it's a number, add directly to postfix, if it's an operand, add to
     * the stack.
     * 
     * @author Benjamin Kearney
     */
    private void convertToPostfix(){
        StringBuilder sb = new StringBuilder();

        for(int i = 0; i < infix.length(); i++){           
            if(Character.isDigit(infix.charAt(i))){
                sb.append(infix.charAt(i));

                while((i+1)!=infix.length()&&(Character.isDigit(infix.charAt(i+1))||infix.charAt(i+1)=='.')){
                    sb.append(infix.charAt(++i));
                }
                postfix.add(sb.toString());
                sb.delete(0, sb.length());
            } else{
                addToStack(infix.charAt(i));
            }
        }
        clearStack();
    }
    
    /**
     * inserts operands to the stack by order of precedence
     * 
     * @author Benjamin Kearney
     * @param input 
     */
    private void addToStack(char input){
        if(stack.isEmpty() || input == '('){
            stack.addLast(input);
        } else{
            if(input == ')'){
                while(!stack.getLast().equals('(')){
                    postfix.add(stack.removeLast().toString());
                }
                stack.removeLast();
            }else{
                if(stack.getLast().equals('(')){
                    stack.addLast(input);
                }else{
                    while(!stack.isEmpty()&&!stack.getLast().equals('(')&&getPrecedence(input)<=getPrecedence(stack.getLast())){
                        postfix.add(stack.removeLast().toString());
                    }
                    stack.addLast(input);
                }
            }
        }
    }
    
    /**
     * returns the precedence of a given operator
     * (the higher the number, the higher the precedence)
     * 
     * @author Benjamin Kearney
     * @param operator
     * @return precedence 
     */
    private int getPrecedence(char op){
        if (op == '+' || op == '-'){
            return 1;
        }else if (op == '*' || op == '/'){
            return 2;
        }else{
            return 0;
        }
    }
    
    /**
     * clears the stack into the postfix
     * 
     * @author Benjamin Kearney
     */
    private void clearStack(){
        while(!stack.isEmpty()){
            postfix.add(stack.removeLast().toString());
        }
    }
    
    /**
     * Checks if the infix expression has any divisions by zero, and throws an 
     * exception if it does.
     * 
     * @author Benjamin Kearney
     * @throws DivideByZeroException 
     */
    private void devideByZeroCheck() throws DivideByZeroException{
        for(int i = 0; i < infix.length(); i++){
            if(i != infix.length()-1 && infix.charAt(i) == '/' && infix.charAt(i+1) == '0'){
                throw new DivideByZeroException();
            }
        }
    }
    
    /**
     * Checks if there are any non-matching parenthesis in the infix expression, 
     * and throws an exception if there are.
     * 
     * @author Benjamin Kearney
     * @throws NonMatchingParenthesisException 
     */
    private void nonMatchingParenthesisCheck() throws NonMatchingParenthesisException{
        int leftCount = 0;
        int rightCount = 0;
        for(int i = 0; i < infix.length(); i++){
            if(infix.charAt(i) == '('){
                leftCount++;
            }
            if(infix.charAt(i) == ')'){
                rightCount++;
                if(rightCount > leftCount){
                    throw new NonMatchingParenthesisException();
                }
            }
        }
        if(leftCount != rightCount){
            throw new NonMatchingParenthesisException();
        }
    }
    
    /**
     * Checks if the expression is valid, and throws an exception if it isn't
     * 
     * @author Benjamin Kearney
     * @throws InvalidExpressionException 
     */
    private void invalidExpressionCheck() throws InvalidExpressionException{
        for(int i = 0; i < infix.length(); i++){
            if(!Character.isDigit(infix.charAt(i)) && infix.charAt(i) != '.' && 
                    infix.charAt(i) != '+' && infix.charAt(i) != '/' && 
                    infix.charAt(i) != '*' && infix.charAt(i) != '-'
                    && infix.charAt(i) != ')' && infix.charAt(i) != '('){
                throw new InvalidExpressionException();
            }
        }
    }
    
    /**
     * Checks if the expression is non-binary, and throws an exception if it is.
     * 
     * @author Benjamin Kearney
     * @throws NonBinaryExpressionException 
     */
    private void nonBinaryCheck() throws NonBinaryExpressionException{
        int numCount = 0;
        int operandCount = 0;
        for(String s : postfix){
            if(Character.isDigit(s.charAt(0))){
                numCount++;
            }else{
                operandCount++;
            }
        }
        if(numCount != operandCount + 1){
            throw new NonBinaryExpressionException();
        }
    }
}
