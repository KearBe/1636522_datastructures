
package com.kearney.datastructures.exceptions;

/**
 *
 * @author Benjamin
 */
public class InvalidExpressionException extends Exception {
    
    public InvalidExpressionException(){
        super("Part(s) of the Expression are neither a number or an operand!");
    }
    
    public InvalidExpressionException(String message){
        super(message);
    }
}
