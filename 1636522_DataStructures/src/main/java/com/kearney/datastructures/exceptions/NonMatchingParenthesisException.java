
package com.kearney.datastructures.exceptions;

/**
 *
 * @author Benjamin
 */
public class NonMatchingParenthesisException extends Exception {
    
    public NonMatchingParenthesisException(){
        super("The Parenthesis Do Not Match Up!");
    }
    
    public NonMatchingParenthesisException(String message){
        super(message);
    }
}
