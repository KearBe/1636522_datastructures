
package com.kearney.datastructures.exceptions;

/**
 *
 * @author Benjamin
 */
public class DivideByZeroException extends Exception {
    
    public DivideByZeroException(){
        super("You Cannot Divide By Zero!");
    }
    
    public DivideByZeroException(String message){
        super(message);
    }
}
