
package com.kearney.datastructures.exceptions;

/**
 *
 * @author Benjamin
 */
public class NonBinaryExpressionException extends Exception {
    
    public NonBinaryExpressionException(){
        super("Not A Binary Expression!");
    }
    
    public NonBinaryExpressionException(String message){
        super(message);
    }
}
