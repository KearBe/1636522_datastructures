package com.kearney.datastructures;

import java.util.*;
import com.kearney.datastructures.Evaluator;
import com.kearney.datastructures.exceptions.*;

import org.junit.Test;
import org.junit.Before;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Benjamin Kearney
 */
@RunWith(Parameterized.class)
public class EvaluatorNonBinaryExpressionTest {
    
    
    @Before
    public void init(){
        
    }
    
    @Parameters
    public static Collection<Object []> data() throws DivideByZeroException, 
            NonMatchingParenthesisException, NonBinaryExpressionException, 
            InvalidExpressionException{
        Queue<String> q = new ArrayDeque<String>();
        Queue<String> q2 = new ArrayDeque<String>();
        Queue<String> q3 = new ArrayDeque<String>();
        
        q.add("70");q.add("+");
        
        q2.add("1700");q2.add("-");q2.add("*");q2.add("2");
        
        q3.add("700");q3.add("/");q3.add("5");q3.add("*");
        
        return Arrays.asList(new Object[][] {
            {q, "0"},
            {q2, "0"},
            {q3, "0"},
        });
    }
    
    private Queue<String> result;
    private String expectedResult;
    
    public EvaluatorNonBinaryExpressionTest(Queue<String> res, String eRes){
        result = res;
        expectedResult = eRes;
    }
    
    @Test(expected = NonBinaryExpressionException.class)
    public void test() throws DivideByZeroException, 
            NonMatchingParenthesisException, NonBinaryExpressionException, 
            InvalidExpressionException{
        assertEquals(expectedResult, new Evaluator(result).getResult());
    }
    
}
