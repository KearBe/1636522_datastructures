package com.kearney.datastructures;

import java.util.*;
import com.kearney.datastructures.Evaluator;
import com.kearney.datastructures.exceptions.*;

import org.junit.Test;
import org.junit.Before;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

/**
 *
 * @author Benjamin Kearney
 */
@RunWith(Parameterized.class)
public class EvaluatorValidDataTest {
    
    
    @Before
    public void init(){
        
    }
    
    @Parameters
    public static Collection<Object []> data() throws DivideByZeroException, NonMatchingParenthesisException, NonBinaryExpressionException, InvalidExpressionException{
        Queue<String> q = new ArrayDeque<String>();
        Queue<String> q2 = new ArrayDeque<String>();
        Queue<String> q3 = new ArrayDeque<String>();
        Queue<String> q4 = new ArrayDeque<String>();
        Queue<String> q5 = new ArrayDeque<String>();
        Queue<String> q6 = new ArrayDeque<String>();
        Queue<String> q7 = new ArrayDeque<String>();
        Queue<String> q8 = new ArrayDeque<String>();
        Queue<String> q9 = new ArrayDeque<String>();
        Queue<String> q10 = new ArrayDeque<String>();
        Queue<String> q11 = new ArrayDeque<String>();
        Queue<String> q12 = new ArrayDeque<String>();
        Queue<String> q13 = new ArrayDeque<String>();
        
        q.add("4.5");q.add("-");q.add("-90");q.add("*");q.add("(");
        q.add("300");q.add("/");q.add("15");q.add(")");q.add("+");
        q.add("2");
        
        q2.add("1700");q2.add("-");q2.add("50");
        q2.add("*");q2.add("(");q2.add("7");q2.add("*");q2.add("8");q2.add(")");
        q2.add("*");q2.add("50.5");
        
        q3.add("(");q3.add("(");q3.add("(");q3.add("(");q3.add("5");q3.add("*");
        q3.add("4");q3.add(")");q3.add(")");q3.add(")");q3.add(")");
        
        q4.add("4");q4.add("/");q4.add("(");q4.add("8");q4.add("+");q4.add("5");
        q4.add("+");q4.add("3");q4.add("-");q4.add("14");q4.add(")");
        
        q5.add("30");q5.add("*");q5.add("50");q5.add("-");q5.add("2");
        
        q6.add("300");q6.add("/");q6.add("10");q6.add("+");q6.add("2");
        q6.add("-");q6.add("(");q6.add("7.50");q6.add(")");
        
        q7.add("-500");q7.add("+");q7.add("-700");
        
        q8.add("30");q8.add("-");q8.add("4");
        
        q9.add("(");q9.add("30");q9.add("-");q9.add("20");q9.add(")");
        q9.add("-");q9.add("(");q9.add("2");q9.add(")");
        
        q10.add("70");q10.add("*");q10.add("(");q10.add("5");q10.add("-");
        q10.add("6");q10.add(")");
        
        q11.add("0");q11.add("*");q11.add("(");q11.add("5");q11.add("*");
        q11.add("700");q11.add("-");q11.add("60");
        q11.add("/");q11.add("3127");q11.add(")");
        
        q12.add("500");q12.add("/");q12.add("25");q12.add("*");q12.add("3");
        q12.add("/");q12.add("10");
        
        q13.add("7");q13.add("+");q13.add("7");q13.add("+");q13.add("(");
        q13.add("7");q13.add("-");q13.add("6");q13.add(")");
        
        return Arrays.asList(new Object[][] {
            {new Evaluator(q).getResult(), "1806.5"},
            {new Evaluator(q2).getResult(), "-139700"},
            {new Evaluator(q3).getResult(), "20"},
            {new Evaluator(q4).getResult(), "2"},
            {new Evaluator(q5).getResult(), "1498"},
            {new Evaluator(q6).getResult(), "24.5"},
            {new Evaluator(q7).getResult(), "-1200"},
            {new Evaluator(q8).getResult(), "26"},
            {new Evaluator(q9).getResult(), "8"},
            {new Evaluator(q10).getResult(), "-70"},
            {new Evaluator(q11).getResult(), "0"},
            {new Evaluator(q12).getResult(), "6"},
            {new Evaluator(q13).getResult(), "15"}
        });
    }
    
    private String result;
    private String expectedResult;
    
    public EvaluatorValidDataTest(String res, String eRes){
        result = res;
        expectedResult = eRes;
    }
    
    @Test
    public void test(){
        assertEquals(expectedResult, result);
    }
    
}
